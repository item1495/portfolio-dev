const header = document.querySelector("header");

const first_skill = document.querySelector(".skill:first-child");
const sk_counters = document.querySelectorAll(".counter span");
const progress_bars = document.querySelectorAll(".skills svg circle");

const ml_section = document.querySelector(".milestones");
const ml_counters = document.querySelectorAll(".number span");

const prt_section = document.querySelector(".portfolio");
const zoom_icons = document.querySelectorAll(".zoom-icon");
const modal_overlay = document.querySelector(".modal-overlay");
const images = document.querySelectorAll(".images img");
const prev_btn = document.querySelector(".prev-btn");
const next_btn = document.querySelector(".next-btn");

const links = document.querySelectorAll(".nav-link");

const toggle_btn = document.querySelector(".toggle-btn");

const hamburger = document.querySelector(".hamburger");

window.addEventListener("scroll", () => {
    activeLink();
    if (!skillsPlayed) skillsCounter();
    if (!mlPlayed) mlCounter();
});

function updateCount(num, maxNum) {
    let currentNum = +num.innerText;

    if (currentNum < maxNum) {
        num.innerText = currentNum + 1;
        setTimeout(() => {
            updateCount(num, maxNum);
        }, 12);
    }
}

/* --------------- Grab elements from DOM --------------- */

/* --------------- Sticky Navbar --------------- */
function stickyNavbar() {
    header.classList.toggle("scrolled", window.pageYOffset > 0);
}

stickyNavbar();

window.addEventListener("scroll", stickyNavbar);

/* --------------- Reveal Animation --------------- */

let sr = ScrollReveal({
    duration: 2500,
    distance: "60px",
});

sr.reveal(".showcase-info", { delay: 600 });
sr.reveal(".showcase-image", { origin: "top", delay: 700 });


/* --------------- Skills Progress Bar Animation --------------- */

function hasReached(el) {
    let topPosition = el.getBoundingClientRect().top;

    if (window.innerHeight >= topPosition + el.offsetHeight) return true;
    return false;
}

let skillsPlayed = false;

function skillsCounter() {
    if (!hasReached(first_skill)) return;

    skillsPlayed = true;

    sk_counters.forEach((counter, i) => {
        let target = +counter.dataset.target;
        let strokeValue = 427 - 427 * (target / 100);

        progress_bars[i].style.setProperty("--target", strokeValue);

        setTimeout(() => {
            updateCount(counter, target)
        }, 400);
    })

    progress_bars.forEach((p) => (p.style.animation = "progress 2s ease-in-out forwards"));
}

skillsCounter();

/* --------------- Services Counter Animation --------------- */

let mlPlayed = false;

function mlCounter() {
    if (!hasReached(ml_section)) return;
    mlPlayed = true;

    ml_counters.forEach((ctr) => {
        let target = +ctr.dataset.target;

        setTimeout(() => {
            updateCount(ctr, target);
        }, 400);
    });
}

mlCounter();

/* --------------- Portfolio Filter Animation --------------- */

let mixer = mixitup(".portfolio-gallery", {
    selectors: {
        target: ".prt-card",
    },
    animation: {
        duration: 500,
    },
});

/* --------------- Modal Pop Up Animation Animation --------------- */

let currentIndex = 0;

zoom_icons.forEach((icn, i) => icn.addEventListener("click", () => {
    prt_section.classList.add("open");
    document.body.classList.add("stopScrolling");
    currentIndex = i;
    changeImage(currentIndex);
})
);

modal_overlay.addEventListener("click", () => {
    prt_section.classList.remove("open");
    document.body.classList.remove("stopScrolling");
});

prev_btn.addEventListener("click", () => {
    if (currentIndex === 0) {
        currentIndex = 5;
    } else {
        currentIndex--;
    }
    changeImage(currentIndex);
});

next_btn.addEventListener("click", () => {
    if (currentIndex === 5) {
        currentIndex = 0;
    } else {
        currentIndex++;
    }
    changeImage(currentIndex);
});

function changeImage(index) {
    images.forEach(img => img.classList.remove("showImage"));
    images[index].classList.add("showImage");
}

/* --------------- Modal Pop Up Animation Animation --------------- */

const swiper = new Swiper('.swiper', {
    loop: true,
    speed: 500,
    autoplay: true,

    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
});

/* --------------- Change Active Link On Scroll --------------- */

function activeLink() {
    let sections = document.querySelectorAll("section[id]");
    let passedSections = Array.from(sections)
        .map((sct, i) => {
            return {
                y: sct.getBoundingClientRect().top - header.offsetHeight,
                id: i,
            };
        })
        .filter((sct) => sct.y <= 0);
    let currSectionID = passedSections.at(-1).id;

    links.forEach(l => l.classList.remove("active"));
    links[currSectionID].classList.add("active");
}

activeLink();

/* --------------- Change Page Theme --------------- */

let firsTheme = localStorage.getItem("dark");

changeTheme(+firsTheme);

function changeTheme(isDark) {
    if (isDark) {
        document.body.classList.add("dark");
        toggle_btn.classList.replace("uil-moon", "uil-sun");
        localStorage.setItem("dark", 1);
    } else {
        document.body.classList.remove("dark");
        toggle_btn.classList.replace("uil-sun", "uil-moon");
        localStorage.setItem("dark", 0);
    }
}

toggle_btn.addEventListener("click", () => {
    changeTheme(!document.body.classList.contains("dark"));
})

/* --------------- Open & Close Navbar Menu --------------- */

hamburger.addEventListener("click", () => {
    document.body.classList.toggle("open");
    document.body.classList.toggle("stopScrolling");
});

links.forEach(link => link.addEventListener("click", () => {
    document.body.classList.remove("open");
    document.body.classList.remove("stopScrolling");
}))

/* --------------- Change Language Web --------------- */

// JavaScript
function CambiarLenguaje() {
    // Obtenemos el nombre de la página actual
    var currentPage = window.location.pathname;

    // Verificamos si la página actual es "index" o "index-en"
    if (currentPage.includes("index")) {
        // Si es "index", redireccionamos a "index-en"
        window.location.href = "index-en.html";

    } else {
        // Si es "index-en", redireccionamos a "index"
        window.location.href = "index-en.html";
    }
}


function changeLanguage() {
    // Obtenemos el nombre de la página actual
    var currentPage = window.location.pathname;

    // Verificamos si la página actual es "index" o "index-en"
    if (currentPage.includes("index")) {
        // Si es "index", redireccionamos a "index-en"
        window.location.href = "index.html";

    } else {
        // Si es "index-en", redireccionamos a "index"
        window.location.href = "index.html";
    }
}

function openInfoProject(projectNumber) {
    var modal = document.getElementById("myInfoProject");
    var image = document.getElementById("projectImage");
    var description = document.getElementById("projectDescription");

    // Actualizar la imagen y la descripción del proyecto
    if (projectNumber === 1) {
        image.src = "assets/proj1.png";
        description.textContent = "El proyecto web fue desarrollado para una empresa que se dedica al turismo, el diseño esta elaborado con Bootstrap, js y plugins adicionales, el diseño es totalmente responsive.";
    } else if (projectNumber === 2) {
        image.src = "assets/proj2.png";
        description.textContent = "El proyecto SIRDC es un sistema que permite controlar todos sus procesos productivos. Posee una aplicacion con IA que le permite conocer si los granos de cacao estan actos para producir chocolate";
    } else if (projectNumber === 3) {
        image.src = "assets/proj3.png";
        description.textContent = "Es una galería interactiva que muestra varias imágenes de tu trabajo de manera dinámica con un diseño elegante, brindando una experiencia visual atractiva para los visitantes.";
    } else if (projectNumber === 4) {
        image.src = "assets/proj4.png";
        description.textContent = "Presenta un diseño interactivo con un menú lateral y contenido principal que muestra información sobre ventas, gastos, ingresos y órdenes recientes. Además, cuenta con un menú derecho que ofrece actualizaciones y análisis de ventas.";
    } else if (projectNumber === 5) {
        image.src = "assets/proj5.png";
        description.textContent = "El proyecto es una tienda en línea de venta de ropa, que ofrece una amplia selección de prendas con diferentes estilos y ocasiones. Los usuarios podrán explorar el catálogo, añadir productos al carrito de compra y realizar transacciones seguras.";
    } else if (projectNumber === 6) {
        image.src = "assets/proj6.png";
        description.textContent = "El proyecto representa una galería de imágenes filtrable con Bootstrap, mostrando diferentes categorías como naturaleza, autos y personas. Cada imagen está acompañada de un título y una descripción.";
    } else if (projectNumber === 7) {
        image.src = "assets/proj7.png";
        description.textContent = "Es un efecto interactivo dinámico para cualquier encabezado de paginas web modernas";
    }


    modal.style.display = "grid";
}

function closeInfoProject() {
    document.getElementById("myInfoProject").style.display = "none";
}

window.onclick = function (event) {
    if (event.target == document.getElementById("myInfoProject")) {
        closeInfoProject();
    }
}

/*

// Función para abrir el info-projects
function openInfoProjects() {
    document.getElementById("myInfoProjects").style.display = "block";
}

// Función para cerrar el info-projects
function closeInfoProjects() {
    document.getElementById("myInfoProjects").style.display = "none";
}

// Cerrar el info-projects al hacer clic fuera de él
window.onclick = function (event) {
    if (event.target == document.getElementById("myInfoProjects")) {
        closeInfoProjects();
    }
}

*/
