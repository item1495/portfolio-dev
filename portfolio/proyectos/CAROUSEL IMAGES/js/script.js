var data = [
    {
        img: "img/big-ben.jpg",
        country: "Reino Unido - UK",
        place: "Big Ben",
        describe: "Una icónica torre del reloj en Londres, conocida por su precisión y belleza arquitectónica."
    },
    {
        img: "img/burj-khalifa.jpg",
        country: "Emiratos Árabes - UAE",
        place: "Burj Khalifa",
        describe: "El edificio más alto del mundo, situado en Dubái, ofrece vistas impresionantes de la ciudad y más allá."
    },
    {
        img: "img/grand-canyon.jpg",
        country: "Estados Unidos - USA",
        place: "Gran Cañón",
        describe: "Un impresionante cañón ubicado en Arizona, famoso por sus paisajes panorámicos y su profundidad imponente."
    },
    {
        img: "img/greece.jpg",
        country: "Grecia - GR",
        place: "Partenón de Grecia",
        describe: "Un antiguo templo griego dedicado a la diosa Atenea, considerado un símbolo de la civilización clásica."
    },
    {
        img: "img/louvre.jpg",
        country: "Francia - FR",
        place: "Louvrea de París",
        describe: "Uno de los museos más grandes y famosos del mundo, alberga una vasta colección de arte y antigüedades"
    },
    {
        img: "img/mt-fuji.jpg",
        country: "Japón - JP",
        place: "Monte Fuji",
        describe: "Un icónico volcán sagrado en Japón, famoso por su forma cónica y su belleza natural."
    }
];

const introduce = document.querySelector(".introduce");
const ordinalNumber = document.querySelector(".ordinal-number");

introduce.innerHTML = "";
ordinalNumber.innerHTML = "";
for (let i = 0; i < data.length; i++) {
    introduce.innerHTML += `
        <div class="wrapper">
            <span>
                <h5 class="country" style="--idx: 0">${data[i].country}</h5>
            </span>
            <span>
                <h1 class="place" style="--idx: 1">${data[i].place}</h1>
            </span>
            <span>
                <p class="describe" style="--idx: 2">${data[i].describe}</p>
            </span>
            <span>
                <button class="discover-button" style="--idx: 3">Discover now</button>
            </span>
        </div>
    `;

    ordinalNumber.innerHTML += `<h2>0${i + 1}</h2>`;
}
introduce.children[0].classList.add("active");
ordinalNumber.children[0].classList.add("active");

const thumbnailListWrapper = document.querySelector(".thumbnail-list .wrapper");

thumbnailListWrapper.innerHTML += `
    <div class="thumbnail zoom">
        <img src="${data[0].img}" alt="">
    </div>
`;

for (let i = 1; i < data.length; i++) {
    thumbnailListWrapper.innerHTML += `
        <div class="thumbnail" style="--idx: ${i - 1}">
            <img src="${data[i].img}" alt="">
        </div>
    `;
}

const nextBtn = document.querySelector(".navigation .next-button");
var currentIndex = 0;

nextBtn.addEventListener("click", () => {
    nextBtn.disabled = true;
    var clone = thumbnailListWrapper.children[0].cloneNode(true);
    clone.classList.remove("zoom");
    thumbnailListWrapper.appendChild(clone);
    thumbnailListWrapper.children[1].classList.add("zoom");
    setTimeout(() => {
        thumbnailListWrapper.children[0].remove();
        nextBtn.disabled = false;
    }, 1000);

    for (let i = 2; i < thumbnailListWrapper.childElementCount; i++) {
        thumbnailListWrapper.children[i].style = `--idx: ${i - 2}`;
    }
    if (currentIndex < data.length - 1) {
        currentIndex++;
    }
    else currentIndex = 0;
    for (let i = 0; i < data.length; i++) {
        introduce.children[i].classList.remove("active");
        ordinalNumber.children[i].classList.remove("active");
    }
    introduce.children[currentIndex].classList.add("active");
    ordinalNumber.children[currentIndex].classList.add("active");
    ordinalNumber.children[currentIndex].textContent = `0${currentIndex + 1}`;
});

