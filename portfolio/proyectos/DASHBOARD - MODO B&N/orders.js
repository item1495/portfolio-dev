const Orders = [{
        productName: 'Foldable Mini Drone',
        productNumber: '85631',
        paymentStatus: 'Due',
        shipping: 'Pending'
    },
    {
        productName: 'Larvender kf102 Drone',
        productNumber: '8563',
        paymentStatus: 'Refunded',
        shipping: 'Declined'
    },
    {
        productName: 'Ruko f11 pro drone',
        productNumber: '8561',
        paymentStatus: 'Due',
        shipping: 'Pending'
    },
    {
        productName: 'Drone with camera ',
        productNumber: '5631',
        paymentStatus: 'Paid',
        shipping: 'Delivered'
    },
    {
        productName: 'GPS 4K DRONE',
        productNumber: '45631',
        paymentStatus: 'Paid',
        shipping: 'Delivered'
    },
    {
        productName: 'GPS 4K DRONE',
        productNumber: '25631',
        paymentStatus: 'Due',
        shipping: 'Delivered'
    }
]